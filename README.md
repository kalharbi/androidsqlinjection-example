SQL Injection Problem:
======================

How to exploit the SQL Injection attack?
----------------------------------------
If the user types the following string into the PIN text field: a' OR balance > '0
, then the app will display all records that have a balance larger than zero.

How to prevent the SQL Injection attack?
----------------------------------------
1. Use Parameterized queries: In the class AccountsDataStore, change the concatenated string query values in the method getUserAccount(String userID, String userPin) to parameterized queries.
2. Use the bulit-in inputType in the editable textfield in the xml layout file. See the activity__main.xml file.
3. Use Regular expressions to sanitize user's input. See the validatePin(String) and validateUserId(String) methods in the MainActivity.java file.