/**
Khalid
*/
package com.kalharbi.sqliteinjection.utils;

public class Constants {
	
	public static final int DB_VERSION = 2;
	public static final String DB_NAME = "accounts.sqlite";
	public static final String ACCOUNT_TABLE_NAME = "account";
	public static final String ACCOUNT_COL_ACCOUNT_ID = "_id";
	public static final String ACCOUNT_COL_USER_ID = "user_id";
	public static final String ACCOUNT_COL_PIN = "pin";
	public static final String ACCOUNT_COL_BALANCE = "balance";
}
