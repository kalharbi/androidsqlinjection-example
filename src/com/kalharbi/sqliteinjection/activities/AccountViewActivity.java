/**
Khalid
 */
package com.kalharbi.sqliteinjection.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.kalharbi.sqliteinjection.utils.Constants;
import com.kalharbi.sqliteinjecttion.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AccountViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		initActivity();
	}

	private void initActivity() {
		final LinearLayout resultsLayout = (LinearLayout) findViewById(R.id.resultsLayout);
		Intent intent = getIntent();
		ArrayList<HashMap<String, String>> userInfo = (ArrayList<HashMap<String, String>>) intent
				.getSerializableExtra("userInfo");

		for (HashMap<String, String> userValues : userInfo) {
			int i = userValues.size();
			for (Map.Entry<String, String> entry : userValues.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();

				if (key.equals(Constants.ACCOUNT_COL_USER_ID)) {

					TextView textViewUserName = new TextView(this);
					textViewUserName.setId(200 + i);
					textViewUserName.setText("User Id:     " + value);
					resultsLayout.addView(textViewUserName);
				} else if (key.equals(Constants.ACCOUNT_COL_BALANCE)) {

					TextView textViewUserBalance = new TextView(this);
					textViewUserBalance.setId(300 + i);
					textViewUserBalance.setText("Balance:     " + value);
					resultsLayout.addView(textViewUserBalance);
				}
			}
		}
	}

}
