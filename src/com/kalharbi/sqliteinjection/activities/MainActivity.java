package com.kalharbi.sqliteinjection.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.kalharbi.sqliteinjection.dbs.AccountsDataStore;
import com.kalharbi.sqliteinjecttion.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private AccountsDataStore accountsDataStore = new AccountsDataStore(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void doClick(View view) {
		if (view.getId() == R.id.button_search) {
			final EditText userIDEditTextView = (EditText) (findViewById(R.id.editText_userid));
			final String userId = userIDEditTextView.getText().toString();

			final EditText pinEditTextView = (EditText) (findViewById(R.id.editText_pin));
			final String userPin = pinEditTextView.getText().toString();

			if (!validatePin(userPin) || !validateUserId(userId)) {
				showLoginFailed();
				return;
			}

			ArrayList<HashMap<String, String>> userInfo = accountsDataStore
					.getUserAccount(userId, userPin);
			if (userInfo.size() != 0) {
				showResultActivity(userInfo);
			} else {
				showLoginFailed();
			}
		}
	}

	private void showResultActivity(ArrayList<HashMap<String, String>> userInfo) {
		Intent intent = new Intent(getApplicationContext(),
				AccountViewActivity.class);
		intent.putExtra("userInfo", userInfo);
		startActivity(intent);
	}

	private boolean validateUserId(String userId) {
		String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Matcher matcher = Pattern.compile(emailPattern).matcher(userId);
		if (matcher.find()) {
			return true;
		}
		return false;
	}

	private boolean validatePin(String pinText) {
		String pinPattern = "^[0-9]{3,20}$";
		Matcher matcher = Pattern.compile(pinPattern).matcher(pinText);
		if (matcher.find()) {
			return true;
		}
		return false;
	}

	private void showLoginFailed() {
		TextView textViewResult = (TextView) (findViewById(R.id.resultTextView));
		textViewResult.setText("login failed.");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
