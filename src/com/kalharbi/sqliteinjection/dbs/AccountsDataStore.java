/**
Khalid
 */
package com.kalharbi.sqliteinjection.dbs;

import java.util.ArrayList;
import java.util.HashMap;

import com.kalharbi.sqliteinjection.utils.Constants;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class AccountsDataStore {
	private static String TAG = AccountsDataStore.class.getSimpleName();

	private DatabaseHelper mDatabaseHelper;

	public AccountsDataStore(Context context) {
		mDatabaseHelper = new DatabaseHelper(context);
	}

	// return a subset of the data
	public ArrayList<HashMap<String, String>> getUserAccount(String userId,
			String userPin) {
		SQLiteDatabase database = mDatabaseHelper.getWritableDatabase();
		ArrayList<HashMap<String, String>> userInfo = new ArrayList<HashMap<String, String>>();
		/*
		 * *****************************************************************************************
		 * WARNING : The commented SQL Query below is an example of SQL
		 * Injection vulnerability. *
		 * ********************************************
		 * **********************************************
		 */

		/*
		 * String selectQuery = "SELECT * FROM " + Constants.ACCOUNT_TABLE_NAME
		 * + " WHERE " + Constants.ACCOUNT_COL_USER_ID + " = '" + userId +
		 * "' AND " + Constants.ACCOUNT_COL_PIN + " = '" + userPin + "'";
		 */

		String selectQuery = "SELECT * FROM " + Constants.ACCOUNT_TABLE_NAME
				+ " WHERE " + Constants.ACCOUNT_COL_USER_ID + " =?" + " AND "
				+ Constants.ACCOUNT_COL_PIN + " =?";

		String[] selectionArgs = { userId, userPin };
		try {
			Cursor cursor = database.rawQuery(selectQuery, selectionArgs);
			if (cursor.moveToFirst()) {
				do {
					HashMap<String, String> userMap = new HashMap<String, String>();
					userMap.put(
							Constants.ACCOUNT_COL_USER_ID,
							cursor.getString(cursor
									.getColumnIndex(Constants.ACCOUNT_COL_USER_ID)));
					userMap.put(
							Constants.ACCOUNT_COL_BALANCE,
							cursor.getString(cursor
									.getColumnIndex(Constants.ACCOUNT_COL_BALANCE)));
					userInfo.add(userMap);
				} while (cursor.moveToNext());

				cursor.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "SQLite Database Error: " + e.getMessage());
		}
		return userInfo;
	}

}
