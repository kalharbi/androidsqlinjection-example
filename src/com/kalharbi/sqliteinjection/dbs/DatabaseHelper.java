/**
Khalid
 */
package com.kalharbi.sqliteinjection.dbs;

import com.kalharbi.sqliteinjection.utils.Constants;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.database.SQLException;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String TAG = DatabaseHelper.class.getSimpleName();

	public DatabaseHelper(Context context) {
		super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		String sqlStatement = "CREATE TABLE " + Constants.ACCOUNT_TABLE_NAME
				+ "(" + Constants.ACCOUNT_COL_ACCOUNT_ID
				+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ Constants.ACCOUNT_COL_USER_ID + " TEXT NOT NULL, "
				+ Constants.ACCOUNT_COL_PIN + " INTEGER NOT NULL, "
				+ Constants.ACCOUNT_COL_BALANCE + " REAL NULL" + ");";
		try {
			database.execSQL(sqlStatement);
			Log.i(TAG, "Database Created:" + Constants.DB_VERSION);
			Log.i(TAG, "Table Created:" + Constants.ACCOUNT_TABLE_NAME);
			insertDefaults(database);
		} catch (SQLException e) {
			Log.e(TAG, "Could not create database.");
		}
	}

	// called when the database version changes. It's usually used to add and
	// drop tables.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String query = "DROP TABLE IF EXISTS " + Constants.ACCOUNT_TABLE_NAME;
		db.execSQL(query);
		Log.i(TAG, "Table dropped:" + Constants.ACCOUNT_TABLE_NAME);
		onCreate(db);
	}

	private void insertDefaults(SQLiteDatabase database) {
		try {
			String sqlStmt = "insert into " + Constants.ACCOUNT_TABLE_NAME
					+ "( " + Constants.ACCOUNT_COL_USER_ID + ", "
					+ Constants.ACCOUNT_COL_PIN + " ,"
					+ Constants.ACCOUNT_COL_BALANCE + ")"
					+ " select 'user1@email.com' AS "
					+ Constants.ACCOUNT_COL_USER_ID + ", '123' AS "
					+ Constants.ACCOUNT_COL_PIN + ", '19.32' AS "
					+ Constants.ACCOUNT_COL_BALANCE
					+ " union select 'ser2@email.com' , '1234', '10.19' "
					+ " union select 'ser3@email.com' , '4567', '20.40' "
					+ " union select 'ser4@email.com' , '8910', '30.34' "
					+ " union select 'ser5@email.com' , '1112', '45.40' "
					+ " union select 'ser6@email.com' , '1213', '24.76' "
					+ " union select 'ser7@email.com' , '1415', '46.78' "
					+ " union select 'ser8@email.com' , '1516', '56.49' "
					+ " union select 'ser9@email.com' , '1617', '20.00' "
					+ " union select 'ser10@email.com' , '1819', '12.99' ";
			database.execSQL(sqlStmt);

		} catch (SQLException e) {
			Log.e(TAG, "Default insertion failed: " + e.getMessage());
		} catch (IllegalStateException e) {
			Log.e(TAG, "Default insertion failed: " + e.getMessage());
		}
	}

}
